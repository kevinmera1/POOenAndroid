package facci.pm.ta2.kevinmera4c.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
